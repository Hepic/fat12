#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "help_functions.h"
#include "fat12.h"

char *read_string(FILE *file, int pos, int len) {
    char *str = (char *)malloc(sizeof(char) * (len + 5));
    memset(str, 0, sizeof(char) * (len + 5));

    fseek(file, pos, SEEK_SET);
    fread(str, sizeof(char), len, file);
    
    rewind(file);
    return str;
}

void write_string(FILE *file, int pos, int len, char *str) {
    fseek(file, pos, SEEK_SET);
    fwrite(str, sizeof(char), len, file);    
    rewind(file);
}

int read_number(FILE *file, int pos, int len) {
    int i, number = 0, mult = 1;
    char chr;
    
    fseek(file, pos, SEEK_SET);
    
    for (i = 0; i < len; ++i) {
        fread(&chr, sizeof(char), 1, file);
        int val = (256 + (int)chr) % 256;

        number += mult * val; 
        mult *= 256;
    }
    
    rewind(file);
    return number;
}

void write_number(FILE *file, int pos, int num) {
    fseek(file, pos, SEEK_SET);
    
    while (num) {
        char chr = (num & ((1 << 8) - 1));
        fwrite(&chr, sizeof(char), 1, file);
        
        num >>= 8;
    }

    rewind(file);
}

int nth_entry(FILE *file, int n) {
    int num = 3 * n / 2, val;

    int fir = read_number(file, 512 + num, 1);
    int sec = read_number(file, 512 + num + 1, 1);
    
    if (n&1) {
        val = ((fir >> 4) & ((1 << 4) - 1)) | (sec << 4);
    } else {
        val = fir | ((sec & ((1 << 4) - 1)) << 8);
    }
    
    return val;
}

void write_nth_entry(FILE *file, int prev_entry, int entry) {
    char chr_1, chr_2;
    int pos = 3 * prev_entry / 2;

    fseek(file, pos + 512, SEEK_SET);

    if (prev_entry&1) { 
        fread(&chr_2, sizeof(char), 1, file);
        
        chr_1 = (entry & ((1 << 4) - 1));
        chr_2 |= (chr_1 << 4);
        
        fseek(file, -1, SEEK_CUR);
        fwrite(&chr_2, sizeof(char), 1, file);
        
        entry >>= 4;
        chr_1 = (entry & ((1 << 8) - 1));
        
        fwrite(&chr_1, sizeof(char), 1, file); 
    } else {
        chr_1 = (entry & ((1 << 8) - 1));
        fwrite(&chr_1, sizeof(char), 1, file);

        entry >>= 8;
        chr_1 = (entry & ((1 << 4) - 1));
        
        fread(&chr_2, sizeof(char), 1, file);
        chr_2 |= chr_1;
        
        fseek(file, -1, SEEK_CUR);
        fwrite(&chr_2, sizeof(char), 1, file);
    }
}

int get_free_bytes(FILE *file) {
    int bytes_per_sect = read_number(file, 11, 2);
    int entry = 2, free_bytes = 0;
    
    while (entry <= 2848) {
        int value = nth_entry(file, entry);
        
        if (value == 0x00) {
            free_bytes += bytes_per_sect;
        }

        ++entry;
    }

    return free_bytes;
}

int get_filesize(FILE *file) {
    int size;

    fseek(file, 0, SEEK_END);
    size = ftell(file); 
    rewind(file);

    return size;
}

void write_date_time(FILE *file, int pos) {
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    int date = tm.tm_mday | (tm.tm_mon << 5) | ((tm.tm_year - 80) << 9);
    write_number(file, pos + 16, date);
    
    int time = (tm.tm_min << 5) | (tm.tm_hour << 11);
    write_number(file, pos + 14, time);
}

int file_exists(FILE *file, char *file_name) {
    int sec, v, bytes_per_sect = read_number(file, 11, 2);

    for (sec = 0; sec < 14; ++sec) {
        for (v = 0; v < 16; ++v) {
            int offset = FIR_SEC_ROOTDIR * bytes_per_sect;
            offset += BYTES_DIR_ENTRY * NUM_DIR_ENTRIES_PER_SEC * sec;
            offset += BYTES_DIR_ENTRY * v;

            char *name = read_string(file, offset, 8);
            remove_spaces(name);
            
            char *extension = read_string(file, offset + 8, 3);
            char *whole_name = (char*)malloc(sizeof(char) * (strlen(name) + strlen(extension) + 5));

            strcpy(whole_name, name);
            strcat(whole_name, ".");
            strcat(whole_name, extension);

            if (!strcmp(whole_name, file_name)) {
                free(name);
                free(extension);
                free(whole_name);
                return 1;
            }
            
            free(name);
            free(extension);
            free(whole_name);
        }
    }

    return 0;
}

char *get_name(const char *str) {
    char *name = (char *)malloc(sizeof(char) * 8);
    int p = 0;
    
    memset(name, 0, sizeof(char) * 8);

    while (*str != '\0' && *str != '.') {
        name[p++] = *str++;
    }
    
    return name;
}

char *get_extension(const char *str) {
    char *extension = (char *)malloc(sizeof(char) * 3);
    int is_ok = 0, p = 0;
    
    memset(extension, 0, sizeof(char) * 3);
    
    while (*str != '\0') {
        if (is_ok) {
            extension[p++] = *str;
        }

        if (*str == '.') {
            is_ok = 1;
        }

        ++str;
    }

    return extension;
}

void string_to_upper(char *str) {
    while (*str != '\0') {
        if (*str >= 'a' && *str <= 'z') {
            *str = 'A' + abs(*str - 'a');
        }

        ++str;
    }
}

void remove_spaces(char *str) {
    while (*str != '\0') {
        if (*str == ' ') {
            *str = '\0';
            break;
        }

        ++str;
    }
}

void perror_exit(const char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}
