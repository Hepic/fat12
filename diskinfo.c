#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "help_functions.h"
#include "fat12.h"

void os_name(FILE *file) {
    char *name = read_string(file, 3, 8);
    
    printf("OS Name: %s\n", name);
    free(name);
}

void volume_label(FILE *file) {
    int sec, v, i, bytes_per_sect = read_number(file, 11, 2);
    char *label = read_string(file, 43, 11);

    printf("Label of the disk: ");
    
    if (*label == ' ') {
        for (sec = 0; sec < 14; ++sec) {
            for (v = 0; v < 16; ++v) {
                int offset = FIR_SEC_ROOTDIR * bytes_per_sect;
                offset += BYTES_DIR_ENTRY * NUM_DIR_ENTRIES_PER_SEC * sec;
                offset += BYTES_DIR_ENTRY * v;

                char *name = read_string(file, offset, 8);
                remove_spaces(name);
                 
                if (*name != 0x00) {
                    int attr = read_number(file, offset + 11, 1);
                    
                    if (attr == 0x08) {
                        for (i = 0; i < 8; ++i) {
                            printf("%c", name[i]);
                        }
                    }
                }

                free(name);
            }
        }
    }

    printf("\n");
    free(label);
}

void total_size_disk(FILE *file) {
    int total_size, bytes_per_sect, total_sect;
    
    bytes_per_sect = read_number(file, 11, 2);
    total_sect = read_number(file, 19, 2);

    total_size = bytes_per_sect * total_sect;

    printf("Total size of the disk: %d bytes\n", total_size);
}

void free_size_disk(FILE *file) {
    int free_size = get_free_bytes(file);
    printf("Free size of the disk: %d bytes\n", free_size);
}

void number_of_files_root_dir(FILE *file) {
    int sec, v, bytes_per_sect, num_of_files = 0;
    bytes_per_sect = read_number(file, 11, 2);

    for (sec = 0; sec < 14; ++sec) {
        for (v = 0; v < 16; ++v) {
            int offset = FIR_SEC_ROOTDIR * bytes_per_sect;
            offset += BYTES_DIR_ENTRY * NUM_DIR_ENTRIES_PER_SEC * sec;
            offset += BYTES_DIR_ENTRY * v;

            char *name = read_string(file, offset, 8);
            remove_spaces(name);
            
            int attr = read_number(file, offset + 11, 1);

            if (*name == 0x00 || *name == 0xE5 || attr == 0x0F || attr == 0x08) {
                free(name);
                continue;
            }

            ++num_of_files;
            free(name);
        }
    }

    printf("The number of files in the root directory (not including subdirectories): %d\n", num_of_files);
}

void number_fats(FILE *file) {
    int num_fats = read_number(file, 16, 1);
    printf("Number of FAT copies: %d\n", num_fats);
}

void sectors_per_fat(FILE *file) {
    int sect_per_fat = read_number(file, 22, 2);
    printf("Sectors per FAT: %d\n", sect_per_fat);
}


int main(int argc, char *argv[]) {
    FILE *file = fopen(argv[1], "rb");
    
    os_name(file);
    volume_label(file);
    total_size_disk(file);
    free_size_disk(file);

    printf("==============\n");
    number_of_files_root_dir(file);
    printf("==============\n");
    
    number_fats(file);
    sectors_per_fat(file);

    fclose(file);
    return 0;
}
