CC = gcc
FLAGS = -g -Wall
INFO_OJBS = diskinfo.o help_functions.o
LIST_OJBS = disklist.o help_functions.o
GET_OJBS = diskget.o help_functions.o
PUT_OJBS = diskput.o help_functions.o
DEPS = help_functions.h fat12.h

run: diskinfo disklist diskget diskput

diskinfo: $(INFO_OJBS)
	$(CC) $(FLAGS) $^ -o $@

disklist: $(LIST_OJBS)
	$(CC) $(FLAGS) $^ -o $@

diskget: $(GET_OJBS)
	$(CC) $(FLAGS) $^ -o $@

diskput: $(PUT_OJBS)
	$(CC) $(FLAGS) $^ -o $@

%.o: %.c $(DEPS)
	$(CC) $(FLAGS) -c $< -o $@

clean:
	rm -rf *.o diskinfo disklist diskget diskput
