#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "help_functions.h"
#include "fat12.h"

void dir_file_info(FILE *file) {
    int sec, v;
    int bytes_per_sect = read_number(file, 11, 2);
    
    for (sec = 0; sec < 14; ++sec) {
        for (v = 0; v < 16; ++v) {
            int offset = FIR_SEC_ROOTDIR * bytes_per_sect;
            offset += BYTES_DIR_ENTRY * NUM_DIR_ENTRIES_PER_SEC * sec;
            offset += BYTES_DIR_ENTRY * v;
            
            char *name = read_string(file, offset, 8);
            remove_spaces(name);

            int attr = read_number(file, offset + 11, 1);

            if (*name == 0x00 || *name == 0xE5 || attr == 0x0F || attr == 0x08) {
                free(name);
                continue;
            }

            char *extension = read_string(file, offset + 8, 3);
            int size_bytes = read_number(file, offset + 28, 4);
            int date = read_number(file, offset + 16, 2);
            int time = read_number(file, offset + 14, 2);
            
            int day = date & ((1 << 5) - 1);
            int month = (date >> 5) & ((1 << 4) - 1);
            int year = ((date >> 9) & ((1 << 7) - 1)) + 1980;
            
            int minute = (time >> 5) & ((1 << 6) - 1);
            int hour = (time >> 11) & ((1 << 5) - 1);
            
            char type;

            if ((attr & 0x10) == 0x10) {   
                type = 'D'; 
            } else {
                type = 'F'; 
            }
            
            printf("%c ", type);
            printf("%10d ", size_bytes);
            printf(" %15s.%s ", name, extension);  
            printf("%02d/%02d/%02d ", day, month, year);  
            printf("%02d:%02d\n", hour, minute);

            free(name);
            free(extension);
        }
    }
}


int main(int argc, char *argv[]) {
    FILE *file = fopen(argv[1], "rb");  
    
    dir_file_info(file);

    fclose(file);
    return 0;
}
