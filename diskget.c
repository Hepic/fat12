#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "help_functions.h"
#include "fat12.h"

void read_file(FILE *file, FILE *file_1, int size_bytes, int cluster) {
    char chr;
    int i, offset;
    
    int bytes_per_sect = read_number(file, 11, 2);
    
    while (size_bytes > 0 && cluster != 0xFFF) {
        offset = bytes_per_sect * (FIR_SEC_DATA + cluster - 2);
        fseek(file, offset, SEEK_SET);
        
        for (i = 0; i < bytes_per_sect; ++i) {
            fread(&chr, sizeof(char), 1, file);
            fwrite(&chr, sizeof(char), 1, file_1);
            
            if (--size_bytes == 0) {
                break;
            }
        }
        
        cluster = nth_entry(file, cluster);
    }
}

void copy_filesystem_local(FILE *file, char *file_name) {
    int sec, v;
    int bytes_per_sect = read_number(file, 11, 2);
    
    string_to_upper(file_name);
    
    for (sec = 0; sec < 14; ++sec) {
        for (v = 0; v < 16; ++v) {
            int offset = FIR_SEC_ROOTDIR * bytes_per_sect;
            offset += BYTES_DIR_ENTRY * NUM_DIR_ENTRIES_PER_SEC * sec;
            offset += BYTES_DIR_ENTRY * v;

            char *name = read_string(file, offset, 8);
            remove_spaces(name);
            
            int attr = read_number(file, offset + 11, 1);

            if (*name == 0x00 || *name == 0xE5 || attr == 0x0F || attr == 0x08) {
                free(name);
                continue;
            }

            char *extension = read_string(file, offset + 8, 3);
            char *whole_name = (char*)malloc(sizeof(char) * (strlen(name) + strlen(extension) + 5));

            strcpy(whole_name, name);
            strcat(whole_name, ".");
            strcat(whole_name, extension);

            if (strcmp(whole_name, file_name)) {
                free(name);
                free(extension);
                free(whole_name);
                continue;
            }

            int size_bytes = read_number(file, offset + 28, 4);
            int cluster = read_number(file, offset + 26, 2);

            FILE *file_1 = fopen(whole_name, "wb");
            read_file(file, file_1, size_bytes, cluster); 

            free(name);
            free(extension);
            free(whole_name);
            fclose(file_1);

            return;
        }
    }

    printf("File not found\n"); 
}


int main(int argc, char *argv[]) {
    FILE *file = fopen(argv[1], "rb");  
    char *file_name = argv[2];
    
    copy_filesystem_local(file, file_name);

    fclose(file);
    return 0;
}
