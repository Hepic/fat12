#ifndef HELP_FUNCTIONS_H
#define HELP_FUNCTIONS_H

#include <stdio.h>

char *read_string(FILE*, int, int);
void write_string(FILE*, int, int, char*);
int read_number(FILE*, int, int);
void write_number(FILE*, int, int);
int nth_entry(FILE*, int);
void write_nth_entry(FILE*, int, int);
int get_free_bytes(FILE*);
int get_filesize(FILE*);
void write_date_time(FILE*, int);
int file_exists(FILE*, char*);
char *get_name(const char*);
char *get_extension(const char*);
void string_to_upper(char*);
void remove_spaces(char*);
void perror_exit(const char*);

#endif
