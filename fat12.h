#ifndef FAT12_H
#define FAT12_H

#define FIR_SEC_ROOTDIR 19
#define FIR_SEC_DATA 33
#define NUM_DIR_ENTRIES_PER_SEC 16
#define BYTES_DIR_ENTRY 32
#define NUM_SEC_FAT 10

#endif
