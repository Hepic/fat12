#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "help_functions.h"
#include "fat12.h"

void write_file(FILE *file, FILE *file_1, int size_bytes, int *first_cluster) {
    int bytes_per_sect = read_number(file, 11, 2);
    int fat_bits = NUM_SEC_FAT * bytes_per_sect * 8 - 24;
    int bits = 0, entry = 2, prev_entry, i;
    char chr;
    *first_cluster = -1;

    while (bits + 12 <= fat_bits && size_bytes > 0) {
        int value = nth_entry(file, entry);
        
        if (value == 0x00) {
            if (*first_cluster == -1) {
                *first_cluster = entry;
            } else {
                write_nth_entry(file, prev_entry, entry);
            }

            prev_entry = entry;
            
            int offset = bytes_per_sect * (FIR_SEC_DATA + entry - 2);  
            fseek(file, offset, SEEK_SET);
            
            for (i = 0; i < bytes_per_sect; ++i) {
                fread(&chr, sizeof(char), 1, file_1);
                fwrite(&chr, sizeof(char), 1, file);
                
                if (--size_bytes == 0) {
                    break;
                }
            }
        }

        ++entry;
        bits += 12;
    }
    
    write_nth_entry(file, prev_entry, 0xFFF);
}

void copy_local_filesystem(FILE *file, char *file_name) {
    FILE *file_1 = fopen(file_name, "rb");

    if (!file_1) {
        printf("File not found\n");
        return;
    }

    string_to_upper(file_name);
    
    char *name = get_name(file_name);
    char *extension = get_extension(file_name);
    
    if (file_exists(file, file_name)) {
        printf("There is already a file with that name\n");
        free(name);
        free(extension);
        fclose(file_1);
        return;
    }

    int size_bytes = get_filesize(file_1);
    int free_bytes = get_free_bytes(file);
    
    if (size_bytes > free_bytes) {
        printf("No enough free space in the disk image");
        free(name);
        free(extension);
        fclose(file_1);
        return;
    }
    
    int sec, v, first_cluster;
    int bytes_per_sect = read_number(file, 11, 2);

    for (sec = 0; sec < 14; ++sec) {
        for (v = 0; v < 16; ++v) {
            int offset = FIR_SEC_ROOTDIR * bytes_per_sect;
            offset += BYTES_DIR_ENTRY * NUM_DIR_ENTRIES_PER_SEC * sec;
            offset += BYTES_DIR_ENTRY * v;

            char *bytes = read_string(file, offset, 8);
            remove_spaces(bytes);
            
            if (*bytes == 0x00 || *bytes == 0xE5) {
                write_string(file, offset, 8, name); 
                write_string(file, offset + 8, 3, extension); 
                write_number(file, offset + 28, size_bytes);
                write_date_time(file, offset);

                write_file(file, file_1, size_bytes, &first_cluster); 
                write_number(file, offset + 26, first_cluster);
                
                free(bytes);
                free(name);
                free(extension);
                fclose(file_1);
                
                return;
            }

            free(bytes);
        }
    }

    free(name);
    free(extension);
    fclose(file_1);
}


int main(int argc, char *argv[]) {
    FILE *file = fopen(argv[1], "rb+");  
    char *file_name = argv[2];
    
    copy_local_filesystem(file, file_name);

    fclose(file);
    return 0;
}
